# postgres db role

## Usage

```
postgres_db__databases:
  db1:
      database: "mydb1"
      username: "db1"
      password: "foobardb1"
  db2:
      database: "mydb2"
      username: "db2"
      password: "foobardb2"
```

Per database options:

* `database`: required
* `username`: required
* `password`: required
* `collate`
* `ctype`
* `encoding`
* `template`
